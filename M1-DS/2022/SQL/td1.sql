
------------------------------------------------------------------ 
-- select
------------------------------------------------------------------ 

select * from patient;

select id_patient, sexe from patient;

select count(*) from patient;

-- Q1 Afficher la table séjour 


-- where
------------------------------------------------------------------ 

select *
from patient 
where sexe = 'M';

select *
from patient
where date_naissance > '1960-01-01';

-- Q2 selectionner les patients de la ville 1


-- Q3 afficher les patients nés après le 31/03/1986


-- AND
------------------------------------------------------------------ 

select *
from patient
where date_naissance > '1960-01-01'
and sexe = 'F';

-- Q4 afficher les séjours commencer après le 01/02/2020 dans l'hopital 1


-- IN
------------------------------------------------------------------ 

select * 
from patient
where id_ville in (1, 2);

-- Q5 afficher les séjours des hôpitaux 1 et 3


-- GROUP BY
------------------------------------------------------------------ 

select sexe, count(*)
from patient
group by sexe;

-- Q6 Compter le nombre de patient par ville


-- Q7 Compter le nombre de séjours par hopital


-- INNER JOIN
------------------------------------------------------------------ 

select *
from patient p inner join ville v 
on p.id_patient = v.id_ville;

-- Q8 Modifier la requête précédente pour n'afficher que l'id_patient et la ville


-- Q9 Afficher les séjours et les hôptiaux dans lesquels ils ont lieu


-- Q10 Compter le nombre de patients par ville en affichant le nom de la ville



-- Q11 Compter le nombre de séjours par hopital en affichant le nom de l'hôpital



-- Q12 Compter le nombre de patients femme
-- par ville en affichant le nom de la ville



-- Q12 Compter le nombre de séjours commençant après le 01/02/2020
-- par hopital en affichant le nom de l'hôpital



-- insert
------------------------------------------------------------------ 

INSERT INTO ville
(id_ville, ville)
VALUES(6, 'Béthune');

-- Q13 Insérer Loos dans la table ville


-- update
------------------------------------------------------------------ 

update ville set ville = 'Lens' where id_ville = 6;

-- Q14 Remplacer le libellé de la ville numéro 7 par Douai



-- delete
------------------------------------------------------------------ 

delete from ville where id_ville = 6;

-- Q15 supprimer la ville numéro 7


